import React, {useState} from "react";

import Goals from "./components/Goals/Goals";
import NewGoals from "./components/NewGoals/NewGoals";

function App(props) {

    const initialGoals = [
        {
            id: 1,
            title: "Do all exercices !"
        },
        {
            id: 2,
            title: "Finish the course !"
        },
        {
            id: 3,
            title: "Take a break !"
        }
    ]

    const [goalsData, setGoalsData] = useState(initialGoals);

    function addGoalsHandler(goals) {
        console.log(goals);
        setGoalsData(prevGoals => {
            return [goals, ...prevGoals];
        });
        console.log(goalsData);
    }

    console.log(initialGoals);
    console.log(goalsData);

    function deleteGoals(id) {

        const refreshGoals = goalsData.filter(item => item.id !== id);
        setGoalsData(refreshGoals);
        // const indiceDeleteGoals = goalsData.findIndex(idElement => idElement.id === id);
        // console.log(id);
        // console.log(indiceDeleteGoals);
        // console.log(goalsData);
        // console.log(initialGoals);
        // console.log(initialGoals);
        // initialGoals.splice(indiceDeleteGoals, id);
        // console.log(initialGoals);
        // setGoalsData()
        // console.log(goalsData.splice(indiceDeleteGoals, id));
        // console.log(goalsData);
        // console.log(refreshGoals);
        // setGoalsData(refreshGoals);


    }

  return (
      <div>
          <NewGoals onAddGoals={addGoalsHandler}/>
          <Goals items={goalsData} onDeleteGoals={deleteGoals}/>
      </div>
  );
}

export default App;
