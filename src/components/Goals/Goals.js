import React from "react";

import './Goals.css';

import GoalsList from "./GoalsList";

function Goals(props) {
    
    function deleteGoalsData(id) {
        props.onDeleteGoals(id);
    }

    return (
        <GoalsList items={props.items} onDeleteGoalsData={deleteGoalsData} />
    );
}

export default Goals;