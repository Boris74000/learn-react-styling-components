import React from "react";

import GoalsItem from "./GoalsItem";
import './GoalsList.css';

function GoalsList(props) {
    // console.log(props.items);
    function deleteGoalsHandler(id) {
        // console.log(id);
        props.onDeleteGoalsData(id);
    }

    return (
        <ul className="goals-list">
            {props.items.map(item =>
                <GoalsItem
                    onDeleteGoals={deleteGoalsHandler}
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    />
            )}
        </ul>
    );
}

export default GoalsList;