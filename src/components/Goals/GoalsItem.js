import React from "react";

import './GoalsItems.css';

function GoalsItem(props) {
    // console.log(props.items);
    function deleteGoalsHandler() {

        // console.log(props.id);

        const id = props.id;

        // console.log(id);

        props.onDeleteGoals(id);
    }
    

    return (
        <li className="goal-item" onClick={deleteGoalsHandler}>
            {props.title}
        </li>
    );
}

export default GoalsItem;