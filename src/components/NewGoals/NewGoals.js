import React from "react";

import './NewGoals.css';
import NewGoalsForm from './NewGoalsForm';
import Card from '../UI/Card.js';

function NewGoals(props) {
    function addGoalsHandler(enteredGoals) {
        const goalsData = {
            id: Math.random().toString(),
            ...enteredGoals
        }

        props.onAddGoals(goalsData);
    };

    return (
        <Card>
            <NewGoalsForm addGoals={addGoalsHandler}/>
        </Card>
    );
}

export default NewGoals;