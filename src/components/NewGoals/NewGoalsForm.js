import React, {useState} from "react";

import './NewGoalsForm.css';

function NewGoalsForm(props) {

    const [enteredGoal, setEnteredGoal] = useState('');

    function goalChangeHandler(e) {
        setEnteredGoal(e.target.value);
    }
    
    function submitHandler(e) {
        e.preventDefault();
        // console.log(enteredGoal);

        const goalsData = {
            title: enteredGoal
        };
        
        setEnteredGoal('');
        
        props.addGoals(goalsData);
    }
    
    return (
        <form onSubmit={submitHandler}>
            <div>
                <label htmlFor="name">Course Goal</label>
                <input type="text" name="name" id="name" value={enteredGoal} required onChange={goalChangeHandler}/>
            </div>
            <button type="submit">Add Goal</button>
        </form>
    );
}

export default NewGoalsForm;